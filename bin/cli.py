#! /usr/bin/env python
from pathlib import Path

import click
from dotenv import load_dotenv

from has_publications.data.dump_data import main as main_dump_data
from has_publications.data.generate_open_datasets import (
    main as main_generate_open_data,
)
from has_publications.data.get_data import main as main_get_data
from has_publications.data.process_data import main as main_process_data
from has_publications.data.publish_open_data import (
    main as main_publish_open_data,
)

# see `.env` for requisite environment variables
load_dotenv()


@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--content-types-path",
    "-c",
    type=click.Path(
        exists=True, dir_okay=False, readable=True, path_type=Path
    ),
    help="Path of the table containing the types that are interesting",
    required=True,
)
@click.option(
    "--output-path",
    "-o",
    type=click.Path(
        exists=False, file_okay=False, readable=True, path_type=Path
    ),
    help="Path of the folder that will contain the downloaded files",
    required=True,
)
def get_data(content_types_path, output_path):
    main_get_data(content_types_path, output_path)


@cli.command()
@click.option(
    "--content-path",
    "-p",
    type=click.Path(exists=True, dir_okay=True, readable=True, path_type=Path),
    help="Path of the folder containing the publication JSON files",
    required=True,
)
@click.option(
    "--output-path",
    "-o",
    type=click.Path(
        exists=False, file_okay=False, readable=True, path_type=Path
    ),
    help="Path of the folder that will contain the processed files",
    required=True,
)
def process_data(content_path, output_path):
    main_process_data(content_path, cleaned_path=output_path)


@cli.command()
@click.option(
    "--processed-path",
    "-p",
    type=click.Path(exists=True, dir_okay=True, readable=True, path_type=Path),
    help="Path of the folder containing the processed JSON files",
    required=True,
)
@click.option(
    "--output-path",
    "-o",
    type=click.Path(
        exists=False, file_okay=False, readable=True, path_type=Path
    ),
    help="Path of the folder that will contain the open data files",
    required=True,
)
def generate_open_data(processed_path, output_path):
    main_generate_open_data(
        processed_path=processed_path, output_path=output_path
    )


@cli.command()
@click.option(
    "--local-path",
    "-l",
    type=click.Path(exists=True, dir_okay=True, readable=True, path_type=Path),
    help="Path of the folder containing the files to be dumped",
    required=True,
)
@click.option(
    "--remote-path",
    "-r",
    type=click.Path(exists=False, readable=False, path_type=str),
    help="Path of the remote folder where to dump the local files",
    required=True,
)
def dump_data(local_path, remote_path):
    main_dump_data(local_path=local_path, remote_path=remote_path)


@cli.command()
@click.option(
    "--open-data-metadata",
    "-m",
    type=click.Path(
        exists=True, dir_okay=False, readable=True, path_type=Path
    ),
    help="Path of the file containing the initial metadata of the files to be uploaded to data.gouv.fr",
    required=True,
)
def publish_open_data(open_data_metadata):
    main_publish_open_data(data_gouv_metadata_path=open_data_metadata)


if __name__ == "__main__":
    cli()
