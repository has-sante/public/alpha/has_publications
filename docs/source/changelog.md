# Changelog

```{eval-rst}
.. note::
    Find the official changelog here_.
.. _here: https://gitlab.has-sante.fr/has-sante/public/alpha/has_publications/releases/
```
