```{include} readme.md
```

```{eval-rst}
.. toctree::
   :hidden:
   :maxdepth: 1

   readme
   Data <data-docs/index>
   CLI usage <cli-usage>
   License <license>
```

```{eval-rst}
.. role:: bash(code)
   :language: bash
   :class: highlight
```
