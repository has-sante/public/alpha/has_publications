# Publications de la HAS

## À propos de cette documentation

La documentation de ce jeu de données suive le concept de _fiche technique pour les jeux de données_ (ou _Datasheets for Datasets_) proposé par [Gebru et al.](https://arxiv.org/abs/1803.09010) Spécifiquement, nous reprenons l'adaptation et traduction au français proposée par Samuel Goëta [ici](https://teamopendata.rg/t/traduction-et-adaptation-du-modele-de-description-des-donnees-datasheet-for-datasets/1400).

Le but de cette fiche technique est de normaliser la documentation à propos de pourquoi un jeu de données a été créé, quelles informations il contient, les tâches pour lesquelles il devrait et ne devrait pas être utilisé, et si cela pourrait soulever des préoccupations d’ordre éthique ou juridique.

## Motivations pour la création du jeu de données

**Pourquoi le jeu de données a-t-il été initialement créé ?** Les données des publications de la HAS sont trouvables programmatiquement à travers de l'API du site web, documenté [ici](https://gitlab.has-sante.fr/has-sante/public/exercices/exercice_nlp/-/blob/master/documentation/README.md). Ce jeu de données propose les publications traitées, nettoyées et enrichies sous forme de fichiers JSON, CSV et XSLX.

**Pour quelles autres tâches le jeu de données pourrait-il être utilisé ?** Créer un graphe de connaissance en liant les publications par rapport aux thématiques, mots clés, dates, etc.

**Quelles sont les utilisations trompeuses du jeu de données ?** L'utilisation des dates de publication ou de mise à jour comme des dates ayant une valeur légale. Ces dates font partie d'un processus métier interne et ne peuvent pas être prises en compte pour des démarches légales.

**Qui a financé ou soutenu la création du jeu de données ?** La HAS.

## Composition du jeu de données

**Que contient le jeu de données principalement ?** Les métadonnées des documents rédigés par les agents de la HAS.

**Dispose-t-on d’un schéma décrivant les variables du jeu de données ?** Oui, [comme ressource dans ce même jeu de données](https://www.data.gouv.fr/fr/datasets/r/1c9d50d3-b98f-45d6-8d88-94b478a25724) et sur [la documentation du projet](https://has-sante.gitlab.io/public/alpha/has_publications/data-docs/index.html).

**Que contient chaque champ du jeu de données ?** Voir les tableaux du _Dictionnaire abrégé_ ci-dessous.

**Est-ce que le contenu du jeu de données dépend de ressources externes ?** Oui, les textes des publications sont disponibles dans un [jeu de donnée complémentaire](https://www.data.gouv.fr/fr/datasets/628f9c55cde04a0912e8d854). Par ailleurs, pour les publications du type _Avis sur les Médicaments_, le champ `reference` fait référence au Code ATC (Anatomical Therapeutic Chemical).

**De quelles garanties dispose-t-on concernant la pérennité de ces ressources ?** Le code ATC est un identifiant de référence [indexé par l'OMS](https://www.whocc.no/atc_ddd_index/).

## Processus de collecte des données

**Comment les données ont été collectées (avec des capteurs, manuellement par des outils informatiques…) ?** Les publications sont rédigées par des agents de la HAS et puis sont versées automatiquement à partir d'un système d'information interne. Certaines publications sont ajoutées manuellement. La collecte se fait par des outils informatiques, via l'API du site.

**Qui a assuré le processus de collecte de données (des agents, des bénévoles, des étudiants…) ?** Des agents de la HAS.

**Quelle a été la période de collecte des données ?** Le jeu de données contient les métadonnées des publications créées à partir de juin 1999.

**Les données ont-elles été collectées directement ou inférées à partir d’autres données ?** Collectées directement.

**Les données ont-elles été collectées sur un échantillon ? Selon quelles méthodes ?** Oui. Nous n'incluons que les publications de certains types, liées à la production des experts de la HAS. Nous écartons les publications liées au système de gestion de documents du site web. Les publications inclues/exclues sont les suivantes :

| **Types inclus**                                              | **Types exclus**                             |
| ------------------------------------------------------------- | -------------------------------------------- |
| Avis sur les Médicaments                                      | Médecin accrédité                            |
| Avis sur les dispositifs médicaux et autres produits de santé | Avis et décisions de la HAS                  |
| Évaluation des technologies de santé                          | Article HAS                                  |
| Recommandation de bonne pratique                              | Médicament                                   |
| Outil d'amélioration des pratiques professionnelles           | Résultat de certification des établissements |
| Guide maladie chronique                                       | Événement de Calendrier                      |
| Guide méthodologique                                          | Lien externe                                 |
| Recommandation en santé publique                              | Article Webzine                              |
| Études et Rapports                                            | Communiqué de presse                         |
| Guide usagers                                                 | Synthèse d'avis et Fiche bon usage           |
| Recommandation vaccinale                                      | Newsletter HAS                               |
|                                                               | Sous-éléments                                |
|                                                               | Glossaire - Terme                            |
|                                                               | Avis sur les Actes                           |
|                                                               | Brève                                        |
|                                                               | Vos interlocuteurs                           |
|                                                               | Faq - Entrée                                 |
|                                                               | Sondage                                      |
|                                                               | Étude d'évaluation économique                |

**Quelles sont les erreurs connues, les limites, les sources de bruit ou de redondances associées à ces données ?**

1. Ils existent des variables avec des limites, évoquées dans le tableau ci-dessous.
2. Ils existent des publications qu'ont été remplacées par une nouvelle version dans le site web.
3. Les dates inclues dans le jeu de données sont relatives à la gestion du site Web. Ces dates ne portent aucune valeur légale.

## Pré-traitement des données

**Comment les données ont-elles nettoyées ou préparées ?**
Pour chaque publication :

1. Suppression des variables liées au système de gestion de documents pour faciliter l'utilisation.
2. Renommage des noms de champs pour expliciter les contenus.
3. Suppression des doublons dans les contenus dus aux erreurs de saisie.
4. Simplification du code HTML pour faciliter sa lecture en gardant l'opérabilité.
5. Ajout une version en Markdown pour les champs contenant du HTML.
6. L'ajout des URLs des ressources référencées.
7. L'ajout de la date où l'API a été consultée.
8. L'ajout d'une valeur haché correspondant aux variables présentes dans la publication. Spécifiquement, une somme MD5 calculée avec tous les champs de la publication, sauf la date de consultation.

**Les données « brutes » ont-elles été conservées ? Sont-elles diffusées ?** Oui, ils sont trouvables via l'API du site web de la HAS.

**L’outil de prétraitement des données est-il disponible ?** Oui, dans [le repo](https://gitlab.has-sante.fr/has-sante/public/alpha/has_publications/-/tree/master) correspondant à ce jeu de données. La documentation de cet outil se trouve [ici](https://has-sante.gitlab.io/public/alpha/has_publications/).

## Diffusion du jeu de données

**Les données sont-elles diffusées en ligne ? Selon quelles modalités (sur un portail open data, un site web, une API…)?** Oui, sur cette page data.gouv.fr. Aussi, ils sont accessibles sans prétraitement via l'API.

**Selon quelle licence les données sont-elles diffusées ?** [License Ouverte version 2.0](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf)

**Des redevances ou des restrictions sont-elles appliquées dans l’accès aux données ?** Non.

## Maintenance du jeu de données

**Qui assure la maintenance du jeu de données ? Comment peut-on contacter cette personne ? Quel est le service responsable du jeu de données ?** La [Mission Data de la HAS](mailto:open-data@has-sante.fr).

**Est-ce que les rôles sont distincts entre la production des données, leur éditorialisation et leur diffusion ?** Oui. Produit par les services HAS. Éditorialisé aussi par les services HAS et le service communication. Diffusé par le service informatique et la Mission Data de la HAS.

**Le jeu de données sera-t-il mis à jour ? Si oui, à quelle fréquence ?** Oui, de manière hebdomadaire (tous les lundis).

**Si les données deviennent obsolètes, comment cette information sera-t-elle communiquée ?** À travers de cette page.

**Est-il possible de contribuer à l’amélioration des données ? Selon quelles modalités ?** Oui, en nous contactant à travers des commentaires de cette page ou directement par e-mail.

## Considérations légales et éthiques

**Si le jeu de données concerne des individus, ont-ils exprimé leur consentement de manière claire ?** Il ne concerne pas des individus.

**Le jeu de données peut-il exposer de manière directe ou indirecte des individus ?** Non à notre connaissance.

**Ces données sont-elles conformes au RGPD ?** Oui.

**Les données peuvent-elles avantager ou désavantager des groupes sociaux ?** Non à notre connaissance.

**Le jeu de données contient-il des informations pouvant être considérées comme inappropriées ou offensantes ?** Non à notre connaissance

# Organisation de fichiers

Deux fichiers ZIP sont mis à disposition avec les données sous trois formats différents. **Nous conseillons fortement d'utiliser le format JSON.**

### Single

Le premier, `has_publications_single.zip`, contient un seul fichier avec toutes les publications à l'intérieur. Nous fournissons trois types de format : CSV, JSON et XLSX (Excel). L'arborescence de ce fichier ZIP est la suivante :

```shell
├── csv
│   └── AllPublications.csv
├── json
│   └── AllPublications.json
└── xlsx
    └── AllPublications.xlsx
```

### Split

Le deuxième fichier, `has_publications_split.zip`, contient un fichier par type de publication (voir table ci-dessus) et par type de format (CSV, JSON, XLSX) :

```shell
├── csv
│   ├── AVISMedicament.csv
│   ├── AVISProduitsEtPrestations.csv
│   ├── ...
│   └── RecommandationsProfessionnelles.csv
├── json
│   ├── AVISMedicament.json
│   ├── AVISProduitsEtPrestations.json
│   ├── ...
│   └── RecommandationsProfessionnelles.json
└── xlsx
    ├── AVISMedicament.xlsx
    ├── AVISProduitsEtPrestations.xlsx
    ├── ...
    └── RecommandationsProfessionnelles.xlsx
```

## Dictionnaire des données abrégé

### Attention

Certains champs dans les données proposées ont des particularités à prendre en compte lors de son utilisation. En général, il peut aussi exister des erreurs de saisi dans certains champs. Lorsque nous connaissons de telles erreurs pour certains champs, nous ajoutons des commentaires dans les tableaux ci-dessous.

### AVISMedicament.json

Veuillez trouver des informations complémentaires pour les Avis sur les Médicaments [ici](https://www.data.gouv.fr/fr/datasets/evaluation-des-medicaments-2022/) dans un autre jeu de données ouvert de la HAS.

| Field                                    | Description                                                       | Commentaire                       |
| ---------------------------------------- | ----------------------------------------------------------------- | --------------------------------- |
| `class`                                  | Type de la publication                                            |                                   |
| `id`                                     | Identifiant de la publication                                     |                                   |
| `title`                                  | Titre de la publication                                           |                                   |
| `typeDavis`                              | Type d'avis                                                       |                                   |
| `syntheseDeLavis`                        | Synthèse de l'avis                                                |                                   |
| `codeCIP`                                | Code Identifiant de Présentation                                  |                                   |
| `natureDeLaDemande`                      | Nature de la demande d'avis                                       | variable à très haute cardinalité |
| `reference`                              | Code Anatomical Therapeutic Chemical (ATC)                        |                                   |
| `fabricant`                              | Fabricant du médicament                                           |                                   |
| `presentation`                           | Présentation du médicament                                        |                                   |
| `avisPrioritaire`                        | Si l'avis es prioritaire ou pas                                   |                                   |
| `accesPrecoce`                           | Si l'avis es de type accès précoce ou pas                         |                                   |
| `documentLinkSet`                        | Ressources (fichiers) liées                                       |                                   |
| `publicationDate`                        | Date de publication                                               | date sans valeur legal            |
| `creationDate`                           | Date de création                                                  | date sans valeur legal            |
| `apiAccessDate`                          | Date d'accès à l'API du site web HAS                              |                                   |
| `categoriesThematiques`                  | Liste des catégories thématiques de la publication                | terminologie interne à la HAS     |
| `liensSiteWeb`                           | Liste des publications HAS liées                                  |                                   |
| `resumeSiteWeb`                          | Résumé présenté sur le site web                                   |                                   |
| `substance`                              | Substance active du médicament                                    |                                   |
| `serviceMedicalRendu`                    | Valeur du Service Médical Rendu                                   |                                   |
| `serviceMedicalRenduTexte`               | Résumé explicatif du SMR                                          |                                   |
| `ameliorationDuServiceMedicalRendu`      | Valeur de l'Amélioration du Service Médical Rendu                 |                                   |
| `ameliorationDuServiceMedicalRenduTexte` | Résumé explicatif de l'Amélioration du Service Médical Rendu      |                                   |
| `page_url`                               | URL de la page de cette publication                               |                                   |
| `md5`                                    | Hash lié aux métadonnées de cette publication (champs précédents) |                                   |

### RecommandationsProfessionnelles.json

| Field                   | Description                                                       | Commentaire                    |
| ----------------------- | ----------------------------------------------------------------- | ------------------------------ |
| `class`                 | Type de la publication                                            |                                |
| `id`                    | Identifiant de la publication                                     |                                |
| `title`                 | Titre de la publication                                           |                                |
| `surtitre`              | Surtitre de la publication                                        | beaucoup de valeurs manquantes |
| `soustitre`             | Sous-titre de la publication                                      | beaucoup de valeurs manquantes |
| `objectifs`             | Objectifs de la recommandation                                    | beaucoup de valeurs manquantes |
| `dateDeValidation`      | Date de validation par le collège                                 |                                |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                                |
| `publicationDate`       | Date de publication                                               | date sans valeur legal         |
| `creationDate`          | Date de création                                                  | date sans valeur legal         |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                                |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS  |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                                |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                                |
| `page_url`              | URL de la page de cette publication                               |                                |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                                |

### AVISProduitsEtPrestations.json

| Field                               | Description                                                       | Commentaire                   |
| ----------------------------------- | ----------------------------------------------------------------- | ----------------------------- |
| `class`                             | Type de la publication                                            |                               |
| `id`                                | Identifiant de la publication                                     |                               |
| `title`                             | Titre de la publication                                           |                               |
| `typeDavis`                         | Type d'avis                                                       |                               |
| `soustitre`                         | Sous-titre de la publication                                      |                               |
| `natureDeLaDemande`                 | Nature de la demande                                              |                               |
| `demandeurFabricant`                | Demandeur ou fabricant du produit                                 |                               |
| `documentLinkSet`                   | Ressources (fichiers) liées                                       |                               |
| `serviceAttendu`                    | Valeur du Service Attendu                                         |                               |
| `serviceAttenduTexte`               | Résumé explicatif du SA                                           |                               |
| `ameliorationDuServiceAttendu`      | Valeur de l'amélioration du Service Attendu                       |                               |
| `publicationDate`                   | Date de publication                                               | date sans valeur legal        |
| `creationDate`                      | Date de création                                                  | date sans valeur legal        |
| `apiAccessDate`                     | Date d'accès à l'API du site web HAS                              |                               |
| `categoriesThematiques`             | Liste des catégories thématiques de la publication                | terminologie interne à la HAS |
| `liensSiteWeb`                      | Liste des publications HAS liées                                  |                               |
| `resumeSiteWeb`                     | Résumé présenté sur le site web                                   |                               |
| `ameliorationDuServiceAttenduTexte` | Résumé explicatif de l'Amélioration du SA                         |                               |
| `page_url`                          | URL de la page de cette publication                               |                               |
| `md5`                               | Hash lié aux métadonnées de cette publication (champs précédents) |                               |

### GuideMedecinALD.json

| Field                   | Description                                                       | Commentaire                   |
| ----------------------- | ----------------------------------------------------------------- | ----------------------------- |
| `class`                 | Type de la publication                                            |                               |
| `id`                    | Identifiant de la publication                                     |                               |
| `title`                 | Titre de la publication                                           |                               |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                               |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                               |
| `publicationDate`       | Date de publication                                               | date sans valeur legal        |
| `creationDate`          | Date de création                                                  | date sans valeur legal        |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                               |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                               |
| `page_url`              | URL de la page de cette publication                               |                               |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                               |

### EtudeEtEnquete.json

| Field                   | Description                                                       | Commentaire            |
| ----------------------- | ----------------------------------------------------------------- | ---------------------- |
| `class`                 | Type de la publication                                            |                        |
| `id`                    | Identifiant de la publication                                     |                        |
| `title`                 | Titre de la publication                                           |                        |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                        |
| `publicationDate`       | Date de publication                                               |                        |
| `creationDate`          | Date de création                                                  | date sans valeur legal |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                        |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | date sans valeur legal |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                        |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                        |
| `page_url`              | URL de la page de cette publication                               |                        |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                        |

### GuideMethodologique.json

| Field                   | Description                                                       | Commentaire                   |
| ----------------------- | ----------------------------------------------------------------- | ----------------------------- |
| `class`                 | Type de la publication                                            |                               |
| `id`                    | Identifiant de la publication                                     |                               |
| `title`                 | Titre de la publication                                           |                               |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                               |
| `publicationDate`       | Date de publication                                               | date sans valeur legal        |
| `creationDate`          | Date de création                                                  | date sans valeur legal        |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                               |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                               |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                               |
| `page_url`              | URL de la page de cette publication                               |                               |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                               |

### GuidePatient.json

| Field                   | Description                                                       | Commentaire                   |
| ----------------------- | ----------------------------------------------------------------- | ----------------------------- |
| `class`                 | Type de la publication                                            |                               |
| `id`                    | Identifiant de la publication                                     |                               |
| `title`                 | Titre de la publication                                           |                               |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                               |
| `publicationDate`       | Date de publication                                               | date sans valeur legal        |
| `creationDate`          | Date de création                                                  | date sans valeur legal        |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                               |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                               |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                               |
| `page_url`              | URL de la page de cette publication                               |                               |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                               |

### EvaluationDesTechnologiesDeSante.json

| Field                   | Description                                                       | Commentaire                    |
| ----------------------- | ----------------------------------------------------------------- | ------------------------------ |
| `class`                 | Type de la publication                                            |                                |
| `id`                    | Identifiant de la publication                                     |                                |
| `title`                 | Titre de la publication                                           |                                |
| `surtitre`              | Surtitre de la publication                                        | beaucoup de valeurs manquantes |
| `soustitre`             | Sous-titre de la publication                                      | beaucoup de valeurs manquantes |
| `objectifs`             | Objectifs de l'étude                                              |                                |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                                |
| `publicationDate`       | Date de publication                                               | date sans valeur legal         |
| `creationDate`          | Date de création                                                  | date sans valeur legal         |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                                |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                |                                |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                                |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                                |
| `page_url`              | URL de la page de cette publication                               |                                |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                                |

### RecommandationVaccinale.json

| Field                   | Description                                                       | Commentaire                   |
| ----------------------- | ----------------------------------------------------------------- | ----------------------------- |
| `class`                 | Type de la publication                                            |                               |
| `id`                    | Identifiant de la publication                                     |                               |
| `title`                 | Titre de la publication                                           |                               |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                               |
| `publicationDate`       | Date de publication                                               | date sans valeur legal        |
| `creationDate`          | Date de création                                                  | date sans valeur legal        |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                               |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                               |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                               |
| `page_url`              | URL de la page de cette publication                               |                               |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                               |

### EvaluationDesPratiques.json

| Field                   | Description                                                       | Commentaire                    |
| ----------------------- | ----------------------------------------------------------------- | ------------------------------ |
| `class`                 | Type de la publication                                            |                                |
| `id`                    | Identifiant de la publication                                     |                                |
| `title`                 | Titre de la publication                                           |                                |
| `surtitre`              | Surtitre de la publication                                        | beaucoup de valeurs manquantes |
| `soustitre`             | Sous-titre de la publication                                      | beaucoup de valeurs manquantes |
| `resumeCourt`           | Résumé court de cette évaluation                                  | beaucoup de valeurs manquantes |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                                |
| `publicationDate`       | Date de publication                                               | date sans valeur legal         |
| `creationDate`          | Date de création                                                  | date sans valeur legal         |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                                |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS  |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                                |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                                |
| `page_url`              | URL de la page de cette publication                               |                                |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                                |

### EvaluationDesProgrammesEtPolitiq.json

| Field                   | Description                                                       | Commentaire                    |
| ----------------------- | ----------------------------------------------------------------- | ------------------------------ |
| `class`                 | Type de la publication                                            |                                |
| `id`                    | Identifiant de la publication                                     |                                |
| `title`                 | Titre de la publication                                           |                                |
| `surtitre`              | Surtitre de la publication                                        | beaucoup de valeurs manquantes |
| `soustitre`             | Sous-titre de la publication                                      | beaucoup de valeurs manquantes |
| `resumeCourt`           | Résumé court de cette évaluation                                  |                                |
| `objectifs`             | Objectifs de l'évaluation                                         |                                |
| `documentLinkSet`       | Ressources (fichiers) liées                                       |                                |
| `publicationDate`       | Date de publication                                               | date sans valeur legal         |
| `creationDate`          | Date de création                                                  | date sans valeur legal         |
| `apiAccessDate`         | Date d'accès à l'API du site web HAS                              |                                |
| `categoriesThematiques` | Liste des catégories thématiques de la publication                | terminologie interne à la HAS  |
| `liensSiteWeb`          | Liste des publications HAS liées                                  |                                |
| `resumeSiteWeb`         | Résumé présenté sur le site web                                   |                                |
| `page_url`              | URL de la page de cette publication                               |                                |
| `md5`                   | Hash lié aux métadonnées de cette publication (champs précédents) |                                |
