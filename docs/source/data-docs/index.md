# Description générale des données

Ci-dessus, les types de publications HAS traitées et le lien vers ses schemas.

## Avis sur les Médicaments
`AVISMedicament.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/AVISMedicament.md
```


## Avis sur les dispositifs médicaux et autres produits de santé
`AVISProduitsEtPrestations.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/AVISProduitsEtPrestations.md
```

## Evaluation des technologies de santé
`EvaluationDesTechnologiesDeSante.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/EvaluationDesTechnologiesDeSante.md
```

## Recommandation de bonne pratique
`RecommandationsProfessionnelles.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/RecommandationsProfessionnelles.md
```

## Outil d'amélioration des pratiques professionnelles
`EvaluationDesPratiques.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/EvaluationDesPratiques.md
```

## Guide maladie chronique
`GuideMedecinALD.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/GuideMedecinALD.md
```

## Guide méthodologique
`GuideMethodologique.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/GuideMethodologique.md
```

## Recommandation en santé publique
`EvaluationDesProgrammesEtPolitiq.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/EvaluationDesProgrammesEtPolitiq.md
```

## Études et Rapports
`EtudeEtEnquete.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/EtudeEtEnquete.md
```

## Guide usagers
`GuidePatient.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/GuidePatient.md
```

## Recommandation vaccinale
`RecommandationVaccinale.json`
```{eval-rst}
.. toctree::
    :maxdepth: 1

    schemas/RecommandationVaccinale.md
```
