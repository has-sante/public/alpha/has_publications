# Schéma de données des Evaluations des Technologies de Sante

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.EvalTechSanteModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
