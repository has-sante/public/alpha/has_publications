# Schéma de données des Avis sur les Médicaments

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.AvisMedicamentModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
