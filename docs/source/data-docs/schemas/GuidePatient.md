# Schéma de données des Guides usagers

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.GuidePatientModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
