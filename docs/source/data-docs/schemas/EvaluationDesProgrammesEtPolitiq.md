# Schéma de données des Recommandations en santé publique

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.EvalProgrammesPolitiqModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
