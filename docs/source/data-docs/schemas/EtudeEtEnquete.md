# Schéma de données des Études et Rapports

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.EtudeEtEnqueteModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
