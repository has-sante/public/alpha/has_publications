# Schéma de données des Recommandations de bonne pratique

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.RecoProfModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
