# Schéma de données des Avis sur les dispositifs médicaux et autres produits de santé

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.AvisProduitsModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
