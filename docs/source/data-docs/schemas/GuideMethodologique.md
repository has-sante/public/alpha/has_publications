# Schéma de données des Guides méthodologiques

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.GuideMethodologiqueModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
