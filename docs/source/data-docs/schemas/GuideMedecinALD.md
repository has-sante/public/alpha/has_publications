# Schéma de données des Guides maladie chronique

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.GuideMedecinModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
