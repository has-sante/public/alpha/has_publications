# Schéma de données des Recommandations vaccinale

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.RecoVaccinaleModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
