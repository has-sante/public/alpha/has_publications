# Schéma de données des Outils d'amélioration des pratiques professionnelles

```{eval-rst}
.. autopydantic_model:: has_publications.data.models.EvalPratiquesModel
   :inherited-members: BaseModel
.. autopydantic_model:: has_publications.data.models.DocumentLinkSetItem
.. autopydantic_model:: has_publications.data.models.LiensSiteWebItem
```
