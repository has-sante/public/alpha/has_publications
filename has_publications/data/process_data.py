import logging
from collections import Counter

from html2text import html2text
from joblib import Parallel, delayed

from has_publications.data.per_class_methods.avis_medicament import (
    avis_sur_les_medicaments_info_extraction,
    fix_type_avis,
)
from has_publications.data.util import (
    do_clean_html,
    get_api_categories,
    get_resolved_url,
    is_html_string,
    load_jsons,
    make_md5,
)

logging.basicConfig(level=logging.INFO)
import json
from pathlib import Path
from typing import Dict, Optional

from tqdm import tqdm

from has_publications.data import html_keys, replacement_keys, to_keep_keys


def load_datasets(content_path: Path):
    if not content_path.exists():
        logging.exception(
            f"Path {content_path} does not exist. Choose another one with JSON files inside."
        )
    if content_path.is_file() and content_path.as_posix().endswith("json"):
        json_files = [content_path]
    else:
        json_files = load_jsons(content_path)

    has_datasets = {}
    for json_file in json_files:
        with open(json_file) as dataset_json:
            has_datasets[json_file.stem] = json.load(dataset_json)
    return has_datasets


def simplify_html(publication: Dict):
    for field, value in publication.items():
        value_is_string = False
        if not field in html_keys or not value:
            continue
        if isinstance(value, str):
            value = [value]
            value_is_string = True

        simple_html = [
            do_clean_html(txt).strip("\n") if is_html_string(txt) else txt
            for txt in value
        ]

        markdown_list = [
            html2text(txt, bodywidth=10_000_000)
            if is_html_string(txt)
            else txt
            for txt in simple_html
        ]
        if value_is_string:
            publication[field] = {
                "simple_html": simple_html[0],
                "markdown": markdown_list[0],
            }
        else:
            publication[field] = {
                "simple_html": simple_html,
                "markdown": markdown_list,
            }


def remove_inconsistencies(publication: Dict, has_publication_type: str):
    if has_publication_type == "AVISMedicament":
        # Get a single type of avis instead of several bc of spelling errors
        fix_type_avis(publication)


def information_extraction_from_html(
    publication: Dict, has_publication_type: str
):
    parsed_info = {}
    if has_publication_type == "AVISMedicament":
        parsed_info = avis_sur_les_medicaments_info_extraction(publication)
    return parsed_info


def enrich_data(
    publication: Dict,
):
    # Apply to all types of publications
    add_url_to_document_links(publication)


def add_metadata_md5(publication: Dict):
    api_access_date = publication.pop("apiAccessDate", None)
    publication["md5"] = make_md5(publication)
    publication["apiAccessDate"] = api_access_date


def add_has_api_access_date(publication: Dict, api_access_date: str):
    publication["apiAccessDate"] = api_access_date


def main(content_path: Path, cleaned_path: Path):
    has_datasets = load_datasets(content_path=content_path)
    api_categories = get_api_categories()
    if not cleaned_path.exists():
        cleaned_path.mkdir()
    pbar = tqdm()
    for (
        has_publication_type,
        has_dataset,
    ) in has_datasets.items():  # Per HAS document type
        pbar.desc = has_publication_type
        logging.info(f"Cleaning HAS dataset {has_publication_type}")
        api_access_date = has_dataset["accessAPIDate"]
        # Per publication in the given document type
        processed_publications = Parallel(n_jobs=-3)(
            delayed(process_data)(
                api_access_date,
                has_publication_type,
                api_categories,
                publication,
            )
            for publication in tqdm(has_dataset["dataSet"], leave=False)
        )
        has_dataset_flat = processed_publications
        logging.info(
            f"Saving cleaned {has_publication_type} dataset in {cleaned_path}"
        )
        save_cleaned_dataset(
            has_dataset_flat, cleaned_path, has_publication_type
        )
        pbar.update(1)


def process_data(
    api_access_date, has_publication_type, api_categories, publication
):
    original_publication = dict(publication)
    remove_website_keys(to_keep_keys, publication, has_publication_type)
    rename_keys(publication, has_publication_type, replacement_keys)
    remove_inconsistencies(publication, has_publication_type)
    remove_class_namespaces(publication)
    flatten_categories(publication, api_categories)
    simplify_html(publication)
    enrich_data(publication)
    add_has_page_link(publication)
    add_has_api_access_date(publication, api_access_date)
    add_metadata_md5(publication)
    return publication


def save_cleaned_dataset(HAS_dataset, cleaned_path, has_doc_type):
    with open(
        cleaned_path.joinpath(f"{has_doc_type}.json"), "w", encoding="utf-8"
    ) as filo:
        json.dump(HAS_dataset, filo, indent=4, ensure_ascii=False)


def remove_metadata(HAS_dataset, metadata_keep_keys):
    HAS_dataset["dataset"] = HAS_dataset.pop("dataSet")
    for k in list(HAS_dataset):
        if k not in metadata_keep_keys:
            HAS_dataset.pop(k)


def add_has_page_link(publication: Dict):
    publication[
        "pageURL"
    ] = f"https://www.has-sante.fr/jcms/{publication['id']}/"


def flatten_categories(publication: Dict, api_categories: Dict):
    # Add category label
    new_categories = []
    if (
        "categoriesThematiques" in publication
        and publication["categoriesThematiques"]
    ):
        for category in publication["categoriesThematiques"]:
            new_categories.append(
                api_categories.get(category["id"], category["id"])
            )

        publication["categoriesThematiques"] = new_categories
    # Remove the class namespace
    publication["class"] = publication["class"].split(".")[-1]


def remove_class_namespaces(publication: Dict):
    """Remove the class namespace of the linked documents bc we do not really need it"""
    for doc_info in [
        "documents",
        "liensSiteWeb",
        "documentLinkSet",
    ]:
        if doc_info in publication:
            if isinstance(publication[doc_info], list):
                for item in publication[doc_info]:
                    if not item:
                        continue
                    if doc_info == "documentLinkSet":
                        item.pop("class")
                        continue
                    item["class"] = item["class"].split(".")[-1]


def add_url_to_document_links(publication: Dict):
    for doc_info in ["documentLinkSet", "liensSiteWeb"]:
        if doc_info in publication:
            if isinstance(publication[doc_info], list):
                for item in [lien for lien in publication[doc_info] if lien]:
                    link_id = item["id"]
                    url = f"https://www.has-sante.fr/jcms/{link_id}"
                    item["url"] = url
                    if doc_info == "documentLinkSet":
                        item["resolvedUrl"] = get_resolved_url(url, timeout=0)


def remove_website_keys(
    to_keep_keys, publication, publication_type: Optional[str] = None
):
    # Remove keys not in dataset_keep_keys
    all_keep_keys = list(to_keep_keys["Common"])
    all_keep_keys.extend(to_keep_keys.get(publication_type, []))
    if len(set(all_keep_keys)) != len(all_keep_keys):
        logging.exception(f"Repeated keys: {Counter(all_keep_keys)}")
        raise Exception(
            "We have repeated keys to remove. In common and in specific types"
            "This should not happen."
        )

    # Add keys if missing (to have consistency among all publications' fields)
    for to_keep in all_keep_keys:
        if to_keep not in publication:
            publication[to_keep] = None

    # Remove keys that do not interest us
    for publication_key in list(publication.keys()):
        if publication_key not in all_keep_keys:
            publication.pop(publication_key)
    pass


# this is bidon
def rename_keys(
    publication: Dict,
    has_publication_type: str,
    replacement_keys: Dict[str, Dict],
):
    # Rename date keys as found in replacement_keys
    replacement_keys_publication = dict(replacement_keys["Common"])
    replacement_keys_publication.update(
        replacement_keys.get(has_publication_type, {})
    )
    for replacement_k, replacement_v in replacement_keys_publication.items():
        if replacement_k not in publication:
            continue
        publication[replacement_v] = publication.pop(replacement_k)
