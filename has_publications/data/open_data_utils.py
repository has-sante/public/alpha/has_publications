import json
import logging
import os
from pathlib import Path
from typing import Dict

import requests
from dotenv import load_dotenv

from has_publications.data.models import (
    AvisMedicamentModel,
    AvisProduitsModel,
    DocumentLinkSetItem,
    EtudeEtEnqueteModel,
    EvalPratiquesModel,
    EvalProgrammesPolitiqModel,
    EvalTechSanteModel,
    GuideMedecinModel,
    GuideMethodologiqueModel,
    GuidePatientModel,
    LiensSiteWebItem,
    PublicationCommonMixin,
    RecoProfModel,
    RecoVaccinaleModel,
)

load_dotenv()

API_BASE_URL = "https://www.data.gouv.fr/api/1"
DATA_GOUV_API_KEY = os.getenv("DATA_GOUV_API_KEY")


def validate_path(file_path: Path):
    if not file_path.exists():
        logging.exception(
            "This %s file does not exist. Indicate another" % file_path
        )
        raise FileNotFoundError
    with open(file_path) as filo:
        description_content = filo.read()
    if not description_content:
        logging.exception(
            "The description file %s is empty. Indicate another." % file_path
        )
        raise RuntimeError


def validate_metadata(metadata: Dict):
    """
    Check that we have the minimum needed info to create a new dataset
    and its resources
    """

    dataset_required_fields = ["title", "description_path"]
    for field in dataset_required_fields:
        if not field in metadata["dataset"] or not metadata["dataset"][field]:
            logging.exception(
                "There is no %s field or it is empty"
                "in the data gouv"
                "metadata JSON file. Can't continue" % field
            )
            raise RuntimeError()
        if field == "description_path":
            validate_path(Path(metadata["dataset"][field]))
    resource_required_fields = ["file_path", "title"]
    for field in resource_required_fields:
        for id_res, resource in enumerate(metadata["resources"]):
            if not field in resource or not resource[field]:
                logging.exception(
                    "There is no %s field in the %sth resource, or"
                    " it is empty in the data gouv "
                    "metadata JSON file. Cannot continue." % (field, id_res)
                )
                raise RuntimeError()
            if "description_path" in resource:
                if (
                    not Path(resource["description_path"]).exists()
                    or not Path(resource["description_path"]).is_file()
                ):
                    logging.warning(
                        "The file indicated %s of the %dth resource does not exist or is not a file."
                        % (resource["description_path"], id_res)
                    )


def create_dataset(metadata_file_path, private: bool = True):
    url = API_BASE_URL + "/datasets/"
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    with open(metadata_file_path, encoding="utf-8") as metadata_file:
        metadata = json.load(metadata_file)

    with open(
        metadata["dataset"]["description_path"], encoding="utf-8"
    ) as description_file:
        description = description_file.read()
    payload = {
        "title": metadata["dataset"]["title"],
        "description": description,
        "organization": metadata["dataset"].get("organization"),
        "private": private,
    }
    response = requests.post(
        url,
        headers=headers,
        json=payload,
        timeout=30,
    )
    response.raise_for_status()
    return response.json()


def save_dataset_metadata(
    data_gouv_metadata_path: Path, dataset_metadata: Dict
):
    with open(data_gouv_metadata_path) as filo:
        data_gouv_metadata = json.load(filo)

    data_gouv_metadata["dataset"]["id"] = dataset_metadata["id"]
    with open(data_gouv_metadata_path, "w") as filo:
        json.dump(data_gouv_metadata, filo, indent=4)


def save_resource_metadata(
    data_gouv_metadata_path: Path, resource_path: str, resource_metadata: Dict
):
    with open(data_gouv_metadata_path) as filo:
        data_gouv_metadata = json.load(filo)
    for resource in data_gouv_metadata["resources"]:
        if resource["file_path"] == resource_path:
            resource["id"] = resource_metadata["id"]
            break
    with open(data_gouv_metadata_path, "w") as filo:
        json.dump(data_gouv_metadata, filo, indent=4)


def create_resource(dataset_id: str, data_file_path: Path):
    url = API_BASE_URL + f"/datasets/{dataset_id}/upload"
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    with open(data_file_path, "rb") as data_file:
        files = {
            "file": data_file,
        }
        response = requests.post(
            url,
            headers=headers,
            files=files,
            timeout=120,
        )
    logging.debug(response.text)
    response.raise_for_status()
    return response.json()


def update_dataset_metadata(
    dataset_id, metadata_file_path, private: bool = True
):
    """
    Met à jour sur data.gouv.fr la description du dataset passée en paramètre
    avec le contenu du fichier au chemin passé en paramètre
    """
    url = API_BASE_URL + f"/datasets/{dataset_id}/"
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    with open(metadata_file_path, encoding="utf-8") as metadata_file:
        metadata = metadata_file.read()
    payload = {"description": metadata, "private": private}
    response = requests.put(
        url,
        headers=headers,
        json=payload,
        timeout=30,
    )
    response.raise_for_status()
    return response.json()


def dataset_exits(url):
    """
    Check if a dataset exists. This is *very* naive :shrugs
    """
    response = requests.get(url)
    if response.status_code == 404:
        return False
    else:
        return True


def update_resource_data(
    dataset_id,
    resource_id,
    data_file_path: Path,
):
    """
    Met à jour sur data.gouv.fr la ressource passée en paramètre avec les données
    présentes sur disque au chemin passé en paramètre. À noter que la ressource
    doit avoir été préalablement créée, cette fonction ne réalise qu'un update.
    """
    url = API_BASE_URL + "/datasets/{}/resources/{}/upload/".format(
        dataset_id, resource_id
    )
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    # TODO we may want to update the title
    with open(data_file_path, "rb") as data_file:
        files = {
            "file": data_file,
        }
        response = requests.post(
            url,
            headers=headers,
            files=files,
            timeout=120,
        )
    response.raise_for_status()
    return response


def update_resource_metadata(
    dataset_id,
    resource_id,
    resource_description_path: Path,
    resource_title: str,
):
    """
    Met à jour sur data.gouv.fr la description du dataset passé en paramètre
    avec le contenu du fichier au chemin passé en paramètre
    """
    url = API_BASE_URL + f"/datasets/{dataset_id}/resources/{resource_id}"
    headers = {
        "X-API-KEY": DATA_GOUV_API_KEY,
    }
    resource_description = ""
    if (
        resource_description_path.exists()
        and resource_description_path.is_file()
    ):
        with open(
            resource_description_path, encoding="utf-8"
        ) as description_file:
            resource_description = description_file.read()

    payload = {"description": resource_description, "title": resource_title}
    response = requests.put(
        url,
        headers=headers,
        json=payload,
        timeout=30,
    )
    response.raise_for_status()
    return response.json()


def generate_json_schemas(json_output_path: Path):
    models = [
        PublicationCommonMixin,
        AvisMedicamentModel,
        RecoProfModel,
        AvisProduitsModel,
        GuideMedecinModel,
        EtudeEtEnqueteModel,
        GuideMethodologiqueModel,
        GuidePatientModel,
        EvalTechSanteModel,
        RecoVaccinaleModel,
        EvalPratiquesModel,
        EvalProgrammesPolitiqModel,
        DocumentLinkSetItem,
        LiensSiteWebItem,
    ]

    models_json = []
    for m in models:
        models_json.append(json.loads(m.schema_json()))
    if json_output_path.parent.exists():
        with open(json_output_path, "w") as filo:
            json.dump(models_json, filo, indent=4)
    else:
        logging.error(
            "Could not store %s file. Parent folder"
            "does not exist." % json_output_path
        )
