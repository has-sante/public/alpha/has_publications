import glob
import hashlib
import json
import logging
import os
import re
from pathlib import Path
from time import sleep
from typing import Dict, Final, List, Union

import requests
from dotenv import load_dotenv

# pylint: disable=no-name-in-module
from lxml.html.clean import Cleaner  # nosec

load_dotenv()

logger: Final[logging.Logger] = logging.getLogger(__name__)

remove_tags = ["table", "tr", "td", "strong"]
cleaner = Cleaner(
    remove_tags=remove_tags, style=True, safe_attrs_only=True, safe_attrs=[]
)


def is_html_string(html_string: str):
    if (
        isinstance(html_string, str)
        and len(html_string)
        and html_string[0] == "<"
        and html_string[-1] == ">"
    ):
        return True
    else:
        return False


def do_clean_html(html_string: str):
    html_clean = cleaner.clean_html(html_string)
    return html_clean


def load_jsons(json_path: Path):
    json_files = [
        Path(path)
        for path in glob.glob(json_path.joinpath("*.json").as_posix())
    ]
    if not json_files:
        logger.exception(
            f"Path {json_path} does not have JSON files inside."
            f" Choose another one with JSON files inside."
        )
        raise FileExistsError(f"There are no JSON files in {json_path}")
    return json_files


def create_folder(path: Union[Path, List[Path]]):
    if not isinstance(path, list):
        path = [path]
    for p in path:
        logging.debug(f"Creating folder {p}")
        if not p.exists():
            p.mkdir(parents=True)


def make_md5(data: Dict):
    data_str = json.dumps(data, sort_keys=True)
    md5_value = hashlib.md5(data_str.encode("utf-8")).hexdigest()  # nosec
    return md5_value


def get_resolved_url(url: str, timeout=0.00001):
    resolved_url = ""
    try:
        r = requests.get(url=url, allow_redirects=True, verify=True)
        html_str = r.content.decode("utf-8")
        extracted_url = re.findall(r"URL='.*(upload/.*)'", html_str)
        if extracted_url:
            resolved_url = f"https://www.has-sante.fr/{extracted_url[0]}"
    except:
        logger.exception(
            "Could not get the resolved URL from this URL %s" % url
        )
    finally:
        sleep(timeout)
    return resolved_url


def get_api_categories():
    jalios_api_key = os.getenv("JALIOS_API_KEY")
    headers = {
        "JaliosJCMS-AuthKey": jalios_api_key,
        "accept": "application/json",
    }
    categories_endpoint = (
        "https://www.has-sante.fr/rest/data/Category?pageSize=2000"
    )
    logger.info("Getting categories id:name info from API")
    r = requests.get(
        categories_endpoint,
        headers=headers,
    )
    if r.status_code != 200:
        raise Exception(f"Categories dict could not be downloaded.")
    json_categories = r.json().get("dataSet", None)
    if json_categories:
        dict_categories = {
            category["id"]: category["name"]
            for category in json_categories
            if category
        }
    else:
        dict_categories = {}
        logger.error("Could not get any categories from API")
    return dict_categories
