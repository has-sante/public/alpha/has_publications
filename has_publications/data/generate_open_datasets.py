import json
import logging
from pathlib import Path
from shutil import copy2, make_archive
from typing import Dict, List, Union

import pandas as pd
from tqdm import tqdm

from has_publications.data.util import create_folder, load_jsons

"""
This script takes as input the processed JSON files (output from proces_data) and generates what will be
published as open data. That is, two types of file organization and three different formats.
File Organization:
    1. Single: A single file with all the publications inside. In the CSV version, only the common
    columns among all the different publication types are kept.
    2. Split: A single file per publication type.
File Formats:
    * Three file extensions are generated: JSON, CSV and XLSX
"""


def main(processed_path: Path, output_path: Path):
    json_files = load_jsons(processed_path)

    # 0. Prepare paths
    single_path = output_path.joinpath("single/")
    split_path = output_path.joinpath("split/")

    split_json = split_path.joinpath("json/")
    split_csv = split_path.joinpath("csv/")
    split_xlsx = split_path.joinpath("xlsx/")

    single_json = single_path.joinpath("json/")
    single_csv = single_path.joinpath("csv/")
    single_xlsx = single_path.joinpath("xlsx/")

    publish_path = output_path.joinpath("publish/")
    create_folder(
        [
            split_json,
            split_csv,
            single_json,
            single_csv,
            split_xlsx,
            single_xlsx,
            publish_path,
        ]
    )

    # Iterate over all processed JSONs and save them in the output folder as CSVs
    dfs_dict = {}
    for json_file in tqdm(json_files, desc="Converting JSON files"):
        json_df = pd.read_json(json_file, encoding="utf-8")
        dfs_dict[json_file.stem] = json_df
        save_csv(json_df, split_csv.joinpath(json_file.stem + ".csv"))
        save_xlsx(json_df, split_xlsx.joinpath(json_file.stem + ".xlsx"))
        copy2(json_file, split_json.joinpath(json_file.name))

    # 1. Create single JSON file with the common fields among all types of documents
    # 1.1 Get a list with the sorted common keys among all publication types
    common_keys = set.intersection(
        *[set(cols.columns) for cols in dfs_dict.values()]
    )
    sorted_common_keys = sorted(
        common_keys, key=list(list(dfs_dict.values())[0].columns).index
    )
    all_publications_df = pd.concat(
        [df[sorted_common_keys] for df in dfs_dict.values()]
    )
    json_list: List[Dict] = json.loads(
        all_publications_df.to_json(orient="records")
    )

    # 2. Save a single dataset with all publication types inside.
    save_csv(all_publications_df, single_csv.joinpath("AllPublications.csv"))
    save_xlsx(
        all_publications_df, single_xlsx.joinpath("AllPublications.xlsx")
    )
    save_json(json_list, single_json.joinpath("AllPublications.json"))

    # 3. Finally, make zips for the single and split folders
    make_zips(
        [single_path, split_path],
        output_folder=publish_path,
        zip_name=["HAS_publications_single", "HAS_publications_split"],
    )


def make_zips(
    folder_to_zip: Union[Path, List[Path]],
    output_folder: Path,
    zip_name: Union[str, List[str]],
):
    if not isinstance(folder_to_zip, list):
        folder_to_zip = [folder_to_zip]
    if not isinstance(zip_name, list):
        zip_name = [zip_name]
    if len(folder_to_zip) != len(zip_name):
        logging.exception(
            "We have a different number of files to zip than"
            "names to name them."
        )
        raise RuntimeError

    for id_, path in enumerate(folder_to_zip):
        zip_path = output_folder.joinpath(zip_name[id_])
        created_file_path = make_archive(
            zip_path.as_posix(), "zip", path.as_posix()
        )
        logging.info("Created file %s." % created_file_path)


def save_json(object_to_save: List, json_path: Path):
    logging.debug(f"Saving  {json_path}")
    with open(json_path, "w", encoding="utf-8") as filo:
        json.dump(object_to_save, filo, indent=4)


def save_csv(df_to_save: pd.DataFrame, csv_path: Path):
    logging.debug(f"Saving {csv_path}")
    df_to_save.replace(
        to_replace=r"\n", value=r"\\n", regex=True, inplace=True
    )
    with open(csv_path, "w", encoding="utf-8-sig") as filo:
        df_to_save.to_csv(
            filo, index=False, line_terminator="\n", encoding="utf-8-sig"
        )


def save_xlsx(df_to_save: pd.DataFrame, xlsx_path: Path):
    logging.debug(f"Saving {xlsx_path}")
    for col in df_to_save.select_dtypes(
        include=["datetime64[ns, UTC]"]
    ).columns:
        df_to_save[col] = df_to_save[col].dt.tz_convert(None)
    df_to_save.to_excel(xlsx_path, index=False)
