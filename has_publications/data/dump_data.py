import logging
import os
from pathlib import Path

import fsspec
from dotenv import load_dotenv

load_dotenv()


def connect_minio():
    minio_key = os.getenv("MINIO_KEY")
    minio_secret_key = os.getenv("MINIO_SECRET_KEY")
    minio_url = os.getenv("MINIO_URL")
    storage_options = {
        "key": minio_key,
        "secret": minio_secret_key,
        "client_kwargs": {"endpoint_url": minio_url},
    }
    fs = fsspec.filesystem("s3", **storage_options)
    return fs


def main(local_path: Path, remote_path: str):
    fs = connect_minio()
    logging.info(f"Dumping {local_path} into {remote_path}")
    fs.put(local_path.as_posix(), remote_path, recursive=True)
