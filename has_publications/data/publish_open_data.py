import json
import logging
from pathlib import Path

import yaml

logging.basicConfig(level=logging.INFO)

from dotenv import load_dotenv

from has_publications.data.open_data_utils import (
    create_dataset,
    create_resource,
    generate_json_schemas,
    save_dataset_metadata,
    save_resource_metadata,
    update_dataset_metadata,
    update_resource_data,
    update_resource_metadata,
    validate_metadata,
)

load_dotenv()


def main(
    data_gouv_metadata_path: Path,
):
    with open(data_gouv_metadata_path, encoding="utf-8") as metadata_file:
        metadata = yaml.safe_load(metadata_file)
        logging.info(
            "Read %s data gouv metadata file OK" % data_gouv_metadata_path
        )
    validate_metadata(metadata)

    dataset_id = metadata["dataset"].get("id")
    dictionary_file = [
        r["file_path"]
        for r in metadata["resources"]
        if "Dictionnaire" in r["title"]
    ][0]
    # Create JSON Data Dictionary
    generate_json_schemas(Path(dictionary_file))
    if not dataset_id:
        # We have do not have an id, we assume the dataset has not been created.
        # We create a new dataset
        logging.info(
            "There is no dataset id in the %s data gouv file. We will create a "
            "new dataset." % data_gouv_metadata_path
        )
        response = create_dataset(metadata_file_path=data_gouv_metadata_path)
        logging.info("Creation of a new dataset on data gouv OK.")
        # We save the newly created dataset metadata (actually just the id for now)
        save_dataset_metadata(data_gouv_metadata_path, response)
        logging.info(
            "Saving new dataset info to data gouv file %s OK"
            % data_gouv_metadata_path
        )
        dataset_id = response["id"]

    else:
        # we have an id in the open data metadata file.
        # so we update the dataset
        logging.info(
            "We have a dataset ID %s in %s. We will update this dataset."
            % (dataset_id, data_gouv_metadata_path)
        )
        update_dataset_metadata(
            dataset_id=dataset_id,
            metadata_file_path=metadata["dataset"]["description_path"],
            private=metadata["dataset"]["private"],
        )
        logging.info("Updating dataset %s OK" % dataset_id)

    # We upload the resources
    for resource in metadata["resources"]:

        resource_id = resource.get("id")
        if not resource_id:  # there is no resource
            logging.info(
                "Dataset %s has a resource (%s) with no id. Adding"
                " a new resource." % (dataset_id, resource["file_path"])
            )

            resource_metadata = create_resource(
                dataset_id, resource["file_path"]
            )
            logging.info(
                "Adding resource (file %s) has been created for dataset %s"
                % (resource["file_path"], dataset_id)
            )
            save_resource_metadata(
                data_gouv_metadata_path=data_gouv_metadata_path,
                resource_path=resource["file_path"],
                resource_metadata=resource_metadata,
            )
            resource_id = resource_metadata["id"]
        else:
            logging.info(
                "Dataset %s has a resource with identifier %s. We will upload the dataset file to data gouv."
                % (dataset_id, resource_id)
            )

            update_resource_data(
                dataset_id, resource_id, resource["file_path"]
            )

        update_resource_metadata(
            dataset_id=dataset_id,
            resource_id=resource_id,
            resource_description_path=Path(resource["description_path"]),
            resource_title=resource["title"],
        )
        logging.info(
            "Updating the metadata description of resource %s OK."
            % resource_id
        )


if __name__ == "__main__":
    main(
        data_gouv_metadata_path=Path("data_gouv_metadata.json"),
    )
