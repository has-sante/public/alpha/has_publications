from datetime import datetime
from enum import Enum
from typing import Any, List, Optional, Union

# pylint: disable=no-name-in-module
from pydantic import BaseModel, Field


class TextContent(BaseModel):
    simple_html: Optional[Union[List[str], str]]
    markdown: Optional[Union[List[str], str]]


class DocumentLinkSetItem(BaseModel):
    """
    Documents liés à la publication
    """

    id: str = Field(
        ..., description="Identifiant du document", regex=r"^\w+_\d+$"
    )
    title: str = Field(..., description="Titre du document")
    url: str = Field(..., description="URL du document (avant redirection)")
    resolvedUrl: str = Field(..., description="URL résolu du document")


class LiensSiteWebItem(BaseModel):
    """
    Liens HAS de la publication
    """

    class_: str = Field(
        ..., alias="class", description="Type de la publication"
    )
    id: str = Field(
        ..., description="Identifiant de la publication", regex=r"^\w+_\d+$"
    )
    title: str = Field(..., description="Titre de la publication")
    url: str = Field(..., description="URL de la publication")


class Class(str, Enum):
    """
    Thèmes possibles pour les classes (types) des publications
    """

    AVISMedicament = "AVISMedicament"
    AVISProduitsEtPrestations = "AVISProduitsEtPrestations"
    EtudeEtEnquete = "EtudeEtEnquete"
    EvaluationDesPratiques = "EvaluationDesPratiques"
    EvaluationDesProgrammesEtPolitiq = "EvaluationDesProgrammesEtPolitiq"
    EvaluationDesTechnologiesDeSante = "EvaluationDesTechnologiesDeSante"
    GuideMedecinALD = "GuideMedecinALD"
    GuideMethodologique = "GuideMethodologique"
    GuidePatient = "GuidePatient"
    RecommandationVaccinale = "RecommandationVaccinale"
    RecommandationsProfessionnelles = "RecommandationsProfessionnelles"


class PublicationCommonMixin(BaseModel):
    class_: Class = Field(
        ..., alias="class", description="Type de la publication"
    )
    id: str = Field(
        ..., description="Identifiant de la publication", regex=r"^\w_\d+$"
    )
    title: str = Field(..., description="Titre de la publication")
    documentLinkSet: List[DocumentLinkSetItem] = Field(
        ..., description="Ressources (fichiers) liées"
    )
    publicationDate: Optional[datetime] = Field(
        ..., description="Date de publication"
    )
    creationDate: Optional[datetime] = Field(
        ..., description="Date de création"
    )
    categoriesThematiques: List[str] = Field(
        ...,
        description="Liste des catégories thématiques de la " "publication",
    )
    liensSiteWeb: Optional[List[Optional[LiensSiteWebItem]]] = Field(
        ..., description="Liste des publications HAS " "liées"
    )
    resumeSiteWeb: Optional[TextContent] = Field(
        ..., description="Résumé présenté sur le site web"
    )
    pageURL: str = Field(
        ..., description="URL de la page de cette publication"
    )
    md5: str = Field(..., description="Hash lié au meta de cette publication")
    apiAccessDate: datetime = Field(
        ..., description="Date d'accès à l'API du site web HAS"
    )


class AvisMedicamentModel(PublicationCommonMixin):
    """
    Schema des données des Avis Medicaments
    """

    typeDavis: str = Field(..., description="Type d'avis")
    syntheseDeLavis: Optional[TextContent] = Field(
        ..., description="Synthèse de l'avis"
    )
    natureDeLaDemande: str = Field(
        ..., description="Nature de la demande d'avis"
    )
    reference: Optional[List[Optional[str]]] = Field(
        ..., description="Code Anatomical Therapeutic Chemical (" "ATC)"
    )
    fabricant: str = Field(..., description="Fabricant du médicament")
    # presentation: TextContent = Field(
    #     ..., description="Présentation du médicament"
    # )
    avisPrioritaire: bool = Field(
        ..., description="Si l'avis est prioritaire ou pas"
    )
    accesPrecoce: bool = Field(
        ..., description="Si l'avis est de type accès précoce ou pas"
    )
    serviceMedicalRendu: Optional[List[str]] = Field(
        ..., description="Valeur du Service Médical Rendu"
    )
    serviceMedicalRenduTexte: Optional[TextContent] = Field(
        ..., description="Résumé explicatif du SMR"
    )
    codeCIP: Optional[List[Optional[str]]] = Field(
        ..., description="Code Identifiant de Présentation"
    )
    substance: List[Optional[str]] = Field(
        ..., description="Substance active du médicament"
    )
    ameliorationDuServiceMedicalRendu: Optional[List[str]] = Field(
        ...,
        description="Résumé explicatif de "
        "l'Amélioration du Service"
        " Médical Rendu",
    )
    ameliorationDuServiceMedicalRenduTexte: Optional[TextContent] = Field(
        ..., description="Valeur de l'Amélioration " "du Service Médical Rendu"
    )
    numeroDeDossierEVAMED: Optional[str] = Field(
        ..., description="Numéro identifiant du dossier dans EVAMED"
    )


# GuideMedecinALD.json
class GuideMedecinModel(PublicationCommonMixin):
    """
    Schema des données des Guides Medecin ALD
    """

    pass


# EtudeEtEnquete.json
class EtudeEtEnqueteModel(PublicationCommonMixin):
    """
    Schema des données des Etudes et enquetes
    """

    pass


# GuideMethodologique.json
class GuideMethodologiqueModel(PublicationCommonMixin):
    """
    Schema des données des Guides Méthodologiques
    """

    pass


# GuidePatient.json
class GuidePatientModel(PublicationCommonMixin):
    """
    Schema des données des Guides Patient
    """

    pass


# EvaluationDesTechnologiesDeSante.json
class EvalTechSanteModel(PublicationCommonMixin):
    """
    Schema des données des Evaluations Technique Santé
    """

    surtitre: Optional[str] = Field(
        ..., description="Surtitre de la publication"
    )
    soustitre: Optional[str] = Field(
        ..., description="Sous-titre de la publication"
    )
    objectifs: Optional[TextContent] = Field(
        ..., description="Objectifs de l'étude"
    )


# AVISProduitsEtPrestations.json
class AvisProduitsModel(PublicationCommonMixin):
    """
    Schema des données des Avis Produits
    """

    typeDavis: str = Field(..., description="Type d'avis")
    sousTitre: Optional[str] = Field(
        ..., description="Sous-titre de la publication"
    )
    natureDeLaDemande: Optional[str] = Field(
        ..., description="Nature de la demande"
    )
    demandeurFabricant: Optional[str] = Field(
        ..., description="Demandeur ou fabricant du produit"
    )

    serviceAttendu: Optional[List[str]] = Field(
        ..., description="Note du Service Attendu"
    )
    serviceAttenduTexte: Optional[TextContent] = Field(
        ..., description="Résumé explicatif du SA"
    )
    ameliorationDuServiceAttendu: Optional[List[str]] = Field(
        ..., description="Note de l'amélioration du Service Attendu"
    )
    ameliorationDuServiceAttenduTexte: Optional[TextContent] = Field(
        ..., description="Résumé explicatif de l'amélioration du SA"
    )


# RecommandationVaccinale.json
class RecoVaccinaleModel(PublicationCommonMixin):
    """
    Schema des données des Recommandations Vaccinal
    """

    pass


# EvaluationDesPratiques.json
class EvalPratiquesModel(PublicationCommonMixin):
    resumeCourt: Optional[str] = Field(
        ..., description="Résumé court de cette évaluation"
    )
    surtitre: Optional[str] = Field(
        ..., description="Surtitre de la publication"
    )
    soustitre: Optional[str] = Field(
        ..., description="Sous-titre de la publication"
    )


# RecommandationsProfessionnelles.json
class RecoProfModel(PublicationCommonMixin):
    """
    Schema des données des Recommandations Professionnelles
    """

    surtitre: Optional[str] = Field(
        ..., description="Surtitre de la publication"
    )
    soustitre: Optional[str] = Field(
        ..., description="Sous-titre de la publication"
    )
    objectifs: Optional[TextContent] = Field(
        ..., description="Objectifs de l'étude"
    )
    dateDeValidation: Optional[datetime] = Field(
        ..., description="Date de validation par le collège"
    )


# EvaluationDesProgrammesEtPolitiq.json
class EvalProgrammesPolitiqModel(PublicationCommonMixin):
    """
    Schema des données des Evaluations des Programmes et Politiques
    """

    surtitre: Optional[str] = Field(
        ..., description="Surtitre de la publication"
    )
    soustitre: Optional[str] = Field(
        ..., description="Sous-titre de la publication"
    )
    resumeCourt: Optional[str] = Field(
        ..., description="Résumé court de cette évaluation"
    )
    objectifs: Optional[TextContent] = Field(
        ..., description="Objectifs de l'étude"
    )
