"""
This module has functions that extract specific information within the publication dict (from the JSON file)
and stores them in a dict to facilitate its access
"""
import logging
from typing import Dict, List

import bs4
import xmltodict

from has_publications.data.util import is_html_string


def fix_type_avis(publication: Dict):
    """
    There are typeDavis within the AvisMedicament publications that have typos when they should all be "Avis sur
    les Médicaments"
    """
    if not "typeDavis" in publication:
        return
    if (
        "Avis sur les " in publication["typeDavis"]
        and publication["typeDavis"] != "Avis sur les Médicaments"
    ) or ("Avis de la Commission" in publication["typeDavis"]):
        publication["typeDavis"] = "Avis sur les Médicaments"


def avis_sur_les_medicaments_info_extraction(publication: Dict):
    """
    Extracts one field from the AVISMedicament publications `resume` or `synthese` fields. Specifically, those
    of type `Avis sur les Médicaments`. These new fields are:
        1. decision: The decision of the avis (favorable, defavorable, maintien, ...)
    """
    # Extract title and avis
    decision: List[str] = []
    extracted_info: Dict[str, List] = {}
    for field in ["resume", "syntheseDeLavis"]:
        if field not in publication or not publication[field]:
            logging.debug(
                f"No %s in publication or value empty in publication %s : %s"
                % (field, publication["id"], publication["class"])
            )
            continue
        for avis_tag in [" favorable", "défavorable"]:
            if avis_tag in publication[field].lower():
                decision.append("favorable")
            if avis_tag in publication[field].lower():
                decision.append("défavorable")
            if avis_tag in publication[field].lower():
                decision.append("maintien")
        if decision:
            extracted_info["avis"] = decision
        if extracted_info:
            break
    return extracted_info
