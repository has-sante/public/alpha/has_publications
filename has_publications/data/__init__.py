common_keys = [
    "class",
    "id",
    # "author",
    # "mainLanguage",
    "pdate",
    "title",
    "cdate",
    # "majorVersion",
    "categories",
    # "abstract",
    # "documentsAssocies",  # documentLinkSet seems like a superset of this
    "liens",
    "documentLinkSet",
    # "accessCount",
    "resume",
]
avis_decision_has = ["objet", "dateDeDecision"]

avis_med_keys = [
    "typeDavis",
    "syntheseDeLavis",
    "natureDeLaDemande",
    "serviceMedicalRendu",
    "serviceMedicalRenduTexte",
    "ameliorationDuServiceMedicalRend",
    "ameliorationDuServiceMedicalRend1",
    "description2",
    "reference",
    "fabricant",
    # "presentation",
    "numeroDeDossierEVAMED",
    "avisPrioritaire",
    "accesPrecoce",
    "codeCIP",
]

resultat_certif_keys = [
    "referenceSIGAES",
    "codeFINESS",
    "surtitre",
    "versionDeCertification",
    "typeDetablissement",
    "entiteJuridique",
    "adresse",
    "codePostal",
    "ville",
    "departement",
    "dateDeCertification",
    "niveauDeDecisionV2014",
    "commentaire",
    "externalLinkSet",
]

article_wysiwyg_keys = [
    "surtitre",
    "soustitre",
    "entete",
    "corps",
    "externalLinkSet",
]
avis_prods_presta_keys = [
    # "date",
    "sousTitre",
    "typeDavis",
    "serviceAttendu",
    "serviceAttenduTexte",
    "ameliorationDuServiceAttendu",
    "ameliorationDuServiceAttenduText",
    "natureDeLaDemande",
    "demandeurFabricant",
    # "numeroDeDossierEVAMED",
]

certificat_accred_keys = [
    "hasid",
    "numeroDinscriptionALordre",
    "civilite",
    "prenom",
    "nom",
    "dateDeFin",
    "nomDeLetablissementDeSante",
    "codePostalDeLetablissementDeSant",
    "villeDeLetablissementDeSante",
    "codeFiness",
    "orgnanismeAgreeConcerne",
]

medicament_keys = [
    "substanceActiveDCI",
    "indicationsConcernees",
    "fabricant",
    "codeATC",
    "presentation",
    "avisSurLesMedicaments",
]

recommandation_prof_keys = [
    "surtitre",
    "soustitre",
    "objectifs",
    "dateDeValidation",
]

evaluation_pratiques_keys = [
    "surtitre",
    "soustitre",
    "resume1",
    # "documents",
]

evaluation_tech_sante_keys = [
    "surtitre",
    "soustitre",
    "objectifs",
    # "dateDeLetude",
]

evaluation_programmes_politq_keys = [
    "surtitre",
    "soustitre",
    "resume1",
    # "dateDeLetude",
    "objectifs",
]

html_keys = [
    "resumeSiteWeb",
    "syntheseDeLavis",
    "serviceMedicalRenduTexte",  # avis medicament
    "ameliorationDuServiceMedicalRenduTexte",  # avis medicament
    "serviceAttenduTexte",  # avis DM
    "ameliorationDuServiceAttenduTexte",  # avis DM
    "presentation",
    "objectifs",
]

metadata_keep_keys = ["dataset", "total"]

to_keep_keys = {
    "Common": common_keys,
    "AVISMedicament": avis_med_keys,
    "AvisEtDecisionsDeLaHAS": avis_decision_has,
    "ResultatDeCertification": resultat_certif_keys,
    "ArticleWYSIWYG": article_wysiwyg_keys,
    "AVISProduitsEtPrestations": avis_prods_presta_keys,
    "CertificatDaccreditation": certificat_accred_keys,
    "Medicament": medicament_keys,
    "RecommandationsProfessionnelles": recommandation_prof_keys,
    "EvaluationDesPratiques": evaluation_pratiques_keys,
    "EvaluationDesTechnologiesDeSante": evaluation_tech_sante_keys,
    "EvaluationDesProgrammesEtPolitiq": evaluation_programmes_politq_keys,
}
replacement_keys = {
    "Common": {
        "cdate": "creationDate",
        # "mdate": "modification_date",
        # "udate": "update_date",  # or upload :shrug
        "pdate": "publicationDate",
        "categories": "categoriesThematiques",
        "liens": "liensSiteWeb",
        "resume": "resumeSiteWeb",
    },
    "AVISMedicament": {
        "description2": "substance",
        # "serviceMedicalRendu": "SMR",
        # "serviceMedicalRenduTexte": "SMRTexte",
        "ameliorationDuServiceMedicalRend": "ameliorationDuServiceMedicalRendu",
        "ameliorationDuServiceMedicalRend1": "ameliorationDuServiceMedicalRenduTexte",
    },
    "AVISProduitsEtPrestations": {
        "ameliorationDuServiceAttenduText": "ameliorationDuServiceAttenduTexte"
    },
    "EvaluationDesPratiques": {"resume1": "resumeCourt"},
    "EvaluationDesProgrammesEtPolitiq": {"resume1": "resumeCourt"},
}
