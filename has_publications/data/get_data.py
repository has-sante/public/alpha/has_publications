import datetime
import json
import logging
import os
from pathlib import Path

import pandas as pd
import requests
from tqdm import tqdm

logging.basicConfig(level=logging.ERROR, format="%(message)s")
logger = logging.getLogger(__name__)

from dotenv import load_dotenv

load_dotenv()


def get_types(
    content_types_path: Path,
    only_interesting_types: bool = False,
    min_freq: int = 1,
):
    if not content_types_path.exists():
        raise FileNotFoundError(
            f"File {content_types_path} does not exist. Please select another"
        )

    df_types = (
        pd.read_excel(content_types_path)
        .fillna(0)
        .astype({"A categoriser": "bool", "Nb. Pub.": int})
    )
    if only_interesting_types:
        df_types = df_types[df_types["A categoriser"]]
    df_types = df_types[df_types["Nb. Pub."] > min_freq]

    interesting_types = list(
        zip(df_types["Nom interne"], [100000] * df_types.shape[0])
    )
    return interesting_types


def download_documents(type_name: str, type_number: int):
    jalios_api_key = os.getenv("JALIOS_API_KEY")
    headers = {
        "JaliosJCMS-AuthKey": jalios_api_key,
        "accept": "application/json",
    }

    r = requests.get(
        f"https://www.has-sante.fr/rest/data/{type_name}?pageSize={type_number}",
        headers=headers,
    )
    if r.status_code != 200:
        logger.exception(
            "Documents of type %s could not be downloaded. Error: %s}"
            % (type_name, r.status_code)
        )
        raise Exception(
            f"Documents of type {type_name} could not be downloaded."
        )

    return r.json()


def save_docs(type_name: str, documents: dict, destination_path: Path):
    if not destination_path.exists():
        logger.warning(
            "Folder %s does not exist. I will create it" % destination_path
        )
        destination_path.mkdir()
    if not documents or "dataSet" not in documents or not documents["dataSet"]:
        logger.warning(f"We did not get any {type_name} publications.")
        return
    with open(destination_path.joinpath(f"{type_name}.json"), "w") as filo:
        json.dump(documents, filo)


def add_access_api_date(downloaded_docs: dict):
    date_iso = (
        datetime.datetime.now(datetime.timezone.utc)
        .astimezone()
        .isoformat(timespec="seconds")
    )
    downloaded_docs["accessAPIDate"] = date_iso


def main(content_types_path: Path, output_path: Path):
    interesting_types = get_types(
        content_types_path=content_types_path, only_interesting_types=True
    )
    stats_dict = {}
    for type_name, number in tqdm(interesting_types):
        logger.info(f"Getting docs of type %s" % type_name)
        downloaded_docs = download_documents(
            type_name=type_name, type_number=number
        )
        add_access_api_date(downloaded_docs)

        stats_dict[type_name] = len(downloaded_docs["dataSet"])
        save_docs(type_name, downloaded_docs, output_path)

    for type_doc, count in stats_dict.items():
        logger.info(
            "Downloaded %d publications of type %s" % (count, type_doc)
        )
