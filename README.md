# Publications de la HAS

**Code Source**: [https://gitlab.has-sante.fr/has-sante/public/alpha/has_publications](https://gitlab.has-sante.fr/has-sante/public/alpha/has_publications)

**Documentation**: [https://has-sante.pages.has-sante.fr/public/alpha/has_publications/](https://has-sante.pages.has-sante.fr/public/alpha/has_publications/)

**Open Dataset**: [https://www.data.gouv.fr/fr/datasets/61e6c823f1eae83eac2cf47b/](https://www.data.gouv.fr/fr/datasets/61e6c823f1eae83eac2cf47b/)

## Aperçu

L'objectif de ce module est de générer un jeu de données avec les métadonnées des certains types de publications de la Haute Autorité de Santé.
À partir de l'API du site web de la HAS, nous extrayons et traitons ces métadonnées pour faciliter sa consultation.

## Motivations pour la création du jeu de données

**Pourquoi le jeu de données a-t-il été initialement créé ?** Les données des publications de la HAS sont trouvables programmatiquement à travers de l'API du site web, documenté [ici](https://gitlab.has-sante.fr/has-sante/public/exercices/exercice_nlp/-/blob/master/documentation/README.md). Ce dataset propose les publications traitées, nettoyées et enrichies sous forme de fichiers JSON, CSV et XSLX.

**Pour quelles autres tâches le jeu de données pourrait-il être utilisé ?** Créer un graphe de connaissance en liant les publications par rapport aux thématiques, mots clés, dates, etc.

**Quelles sont les utilisations trompeuses du jeu de données ?** L'utilisation des dates de publication ou de mise à jour comme des dates ayant une valeur légale. Ces dates font partie d'un processus métier interne et ne peuvent pas être prises en compte pour des démarches légales.

**Qui a financé ou soutenu la création du jeu de données ?** La HAS.

## Composition du jeu de données

**Que contient le jeu de données principalement ?** Les métadonnées des documents rédigés par les agents de la HAS.

**Dispose-t-on d’un schéma décrivant les variables du jeu de données ?** Oui, [comme ressource dans ce même dataset](https://www.data.gouv.fr/fr/datasets/r/1c9d50d3-b98f-45d6-8d88-94b478a25724) et sur [la documentation du projet](https://has-sante.gitlab.io/public/alpha/has_publications/data-docs/index.html).

**Que contient chaque champ du jeu de données ?** Voir les tableaux du _Dictionnaire abrégé_ ci-dessous.

**Est-ce que le contenu du jeu de données dépend de ressources externes ?** Oui, pour les publications du type _Avis sur les Médicaments_, le champ `reference` fait référence au Code ATC (Anatomical Therapeutic Chemical) **De quelles garanties dispose-t-on concernant la pérennité de ces ressources ?** Le code ATC est un identifiant de référence [indexé par l'OMS](https://www.whocc.no/atc_ddd_index/).

## Processus de collecte des données

**Comment les données ont été collectées (avec des capteurs, manuellement par des outils informatiques…) ?** Les publications sont rédigées par des agents de la HAS et puis sont versées automatiquement à partir d'un système d'information interne. Certaines publications sont ajoutées manuellement. La collecte se fait par des outils informatiques, via l'API du site.

**Qui a assuré le processus de collecte de données (des agents, des bénévoles, des étudiants…) ?** Des agents de la HAS.

**Quelle a été la période de collecte des données ?** Le dataset contient les métadonnées des publications créées à partir de juin 1999.

**Les données ont-elles été collectées directement ou inférées à partir d’autres données ?** Collectées directement.

**Les données ont-elles été collectées sur un échantillon ? Selon quelles méthodes ?** Oui. Nous incluions que les types des publications liées à la production des experts de la HAS. Nous écartons les publications liées au système de gestion de documents du site web. Les publications inclues/exclues sont les suivantes :

| **Types inclus**                                              | **Types exclus**                             |
| ------------------------------------------------------------- | -------------------------------------------- |
| Avis sur les Médicaments                                      | Médecin accrédité                            |
| Avis sur les dispositifs médicaux et autres produits de santé | Avis et décisions de la HAS                  |
| Évaluation des technologies de santé                          | Article HAS                                  |
| Recommandation de bonne pratique                              | Médicament                                   |
| Outil d'amélioration des pratiques professionnelles           | Résultat de certification des établissements |
| Guide maladie chronique                                       | Événement de Calendrier                      |
| Guide méthodologique                                          | Lien externe                                 |
| Recommandation en santé publique                              | Article Webzine                              |
| Études et Rapports                                            | Communiqué de presse                         |
| Guide usagers                                                 | Synthèse d'avis et Fiche bon usage           |
| Recommandation vaccinale                                      | Newsletter HAS                               |
|                                                               | Sous-éléments                                |
|                                                               | Glossaire - Terme                            |
|                                                               | Avis sur les Actes                           |
|                                                               | Brève                                        |
|                                                               | Vos interlocuteurs                           |
|                                                               | Faq - Entrée                                 |
|                                                               | Sondage                                      |
|                                                               | Étude d'évaluation économique                |

**Quelles sont les erreurs connues, les limites, les sources de bruit ou de redondances associées à ces données ?**

1. Ils existent des variables avec des limites, évoquées dans le tableau ci-dessous.
2. Ils existent des publications qu'ont été remplacées par une nouvelle version dans le site web.
3. Les dates incluent dans le dataset sont relatives à la gestion du site Web. Ces dates ne portent aucune valeur légale.

## Pré-traitement des données

**Comment les données ont-elles nettoyées ou préparées ?**
Pour chaque publication :

1. Suppression des variables liées au système de gestion de documents pour faciliter l'utilisation.
2. Renommage des noms de champs pour expliciter les contenus.
3. Suppression des doublons dans les contenus dus aux erreurs de saisie.
4. Simplification du code HTML pour faciliter sa lecture en gardant l'opérabilité.
5. Ajout une version en Markdown pour les champs contenant du HTML.
6. L'ajout des URLs des ressources référencées.
7. L'ajout de la date où l'API a été consultée.
8. L'ajout d'une valeur haché correspondant aux variables présentes dans la publication. Spécifiquement, une somme MD5 calculée avec tous les champs de la publication, sauf la date de consultation.

**Les données « brutes » ont-elles été conservées ? Sont-elles diffusées ?** Oui, ils sont trouvables via l'API du site web de la HAS.

**L’outil de prétraitement des données est-il disponible ?** Oui, dans [le repo](https://gitlab.has-sante.fr/has-sante/public/alpha/has_publications/-/tree/master) correspondant à ce dataset. La documentation de cet outil se trouve [ici](https://has-sante.gitlab.io/public/alpha/has_publications/).

## Diffusion du jeu de données

**Les données sont-elles diffusées en ligne ? Selon quelles modalités (sur un portail open data, un site web, une API…)?** Oui, sur cette page data.gouv.fr. Aussi, ils sont accessibles sans prétraitement via l'API.

**Selon quelle licence les données sont-elles diffusées ?** [License Ouverte version 2.0](https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf)

**Des redevances ou des restrictions sont-elles appliquées dans l’accès aux données ?** Non.

## Maintenance du jeu de données

**Qui assure la maintenance du jeu de données ? Comment peut-on contacter cette personne ? Quel est le service responsable du jeu de données ?** La [Mission Data de la HAS](mailto:open-data@has-sante.fr).

**Est-ce que les rôles sont distincts entre la production des données, leur éditorialisation et leur diffusion ?** Oui. Produit par les services HAS. Éditorialisé aussi par les services HAS et le service communication. Diffusé par le service informatique et la Mission Data de la HAS.

**Le jeu de données sera-t-il mis à jour ? Si oui, à quelle fréquence ?** Oui, de manière hebdomadaire (tous les lundis).

**Si les données deviennent obsolètes, comment cette information sera-t-elle communiquée ?** À travers de cette page.

**Est-il possible de contribuer à l’amélioration des données ? Selon quelles modalités ?** Oui, en nous contactant à travers des commentaires de cette page ou directement par e-mail.

## Considérations légales et éthiques

**Si le jeu de données concerne des individus, ont-ils exprimé leur consentement de manière claire ?** Il ne concerne pas des individus.

**Le jeu de données peut-il exposer de manière directe ou indirecte des individus ?** Non à notre connaissance.

**Ces données sont-elles conformes au RGPD ?** Oui.

**Les données peut-elles avantager ou désavantager des groupes sociaux ?** Non à notre connaissance.

**Le jeu de données contient-il des informations pouvant être considérées comme inappropriées ou offensantes ?** Non à notre connaissance

## Execution du projet

> 📝 **Note**
> Toutes les commandes qui suivent sont exécutées à la _racine_ du projet.
> On suppose que `make` est installé sur le système

On peut executer ce projet ainsi:

### Localement en utilisant poetry

Par exemple :

```shell script
make provision-environment # Note: installs ALL dependencies!
poetry shell # Activate the project's virtual environment
jupyter notebook # Launch the Jupyter server
cli main # Run the project main entrypoint
```

### En utilisant Docker

Exécutez :

```shell script
make docker-run
```

Cette commande va construire l'image docker et executer (dans le container docker) l'entrypoint situé dans `bin/entrypoint`.

# Développement

> 📝 **Note**
> Une partie des étapes ci-dessous, sont encapsulées dans le `Makefile` pour faciliter leur utilisation.

> 🔥 **Tip**
> En invoquant `make` sans aucun argument, une liste des commandes disponibles sera affichée.

## Packets et gestion des dépendances

Assurez-vous d'avoir `Python 3.8` et [poetry](https://python-poetry.org/) installés configurés.

Pour installer le projet ainsi que toutes les dépendances (incluant celles de dev):

```shell script
make provision-environment
```

> 🔥 **Tip**
> Exécuter cette commande sans poetry affichera un message d'erreur utile, vous indiquant de l'installer.

## Utilisation de Docker

Les commandes `make` suivantes permettent d'utiliser le projet avec docker facilement :

```shell
docker-build        Build evamed container
docker-rm           Stop container forcefully (i.e., when keyboard interrupts are disabled)
docker-jupyter-run  Runs the Dockerfile with the jupyter entrypoint
docker-run          Runs the Dockerfile with the default entrypoint
docker-run-interactive Runs the Dockerfile with a bash entrypoint
```

Le Dockerfile a été conçu pour être peu couplée aux détails du projet en lui même.
En utilisant des entrypoints différents, il est possible de changer le comportement des containers sans modifier le Dockerfile en lui même.

## Tests

On utilise [pytest](https://pytest.readthedocs.io/) pour les tests unitaires.

Pour lancer les tests, utilisez :

```shell script
make test
```

## Qualité du code

On utilise [pre-commit](https://pre-commit.com/) pour orchestrer les différentes étapes de linting.

Pour lancer le linting, utilisez :

```shell script
make lint
```

> 🚨 **Danger**
> La CI cassera si les tests ou le linting ne passent pas.
> Il est donc recommandé d'executer les checks avant de pousser les changements.

### Automatisation avec les pre-commit-hooks

Pour lancer le linting avant chaque commit, utilisez:

```shell script
make install-pre-commit-hooks
```

> ⚠️ Attention !
> Si les vérifications ne passent pas, cela empêchera le commit.
> Certains checks peuvent modifier automatiquement des fichiers (`isort` et `black` notamment).
> Dans ce cas, il suffit de stagé les changements et de refaire `git commit`

## Documentation

Pour générer la documentation, utilisez :

```shell script
make docs-clean docs-html
```

> 📝 **Note**
> Cette commande va générer des fichiers html dans `docs/_build/html`.
> La page d'accueil de la doc est `docs/_build/html/index.html`.
