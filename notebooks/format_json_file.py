# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.13.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown]
# ## JSON file formatter
# The JSON files produced by the HAS API are unreadable (single line).
# VSCode can't handle "big" files (50Mo quoi :/ ).
# I format it here.

# %%
# !pwd

# %%
import json

# %%
file_path = "../data/raw/AVISMedicament.json"
output_path = "../data/raw/AVISMedicament_formatted.json"

# %%
with open(file_path) as filo:
    json_file = json.load(filo)

# %%
with open(output_path, "w") as filo:
    json.dump(json_file, filo, indent=4)


# %%
