"""Test cases for the main module."""
import json

from has_publications.data.models import (
    AvisMedicamentModel,
    AvisProduitsModel,
    EtudeEtEnqueteModel,
    EvalPratiquesModel,
    EvalProgrammesPolitiqModel,
    EvalTechSanteModel,
    GuideMedecinModel,
    GuideMethodologiqueModel,
    GuidePatientModel,
    RecoProfModel,
    RecoVaccinaleModel,
)


def test_validate_avis_medicament():
    with open("data/processed/AVISMedicament.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            AvisMedicamentModel.parse_obj(publi)


def test_validate_guide_medecin():
    with open("data/processed/GuideMedecinALD.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            GuideMedecinModel.parse_obj(publi)


def test_validate_etude_enquete():
    with open("data/processed/EtudeEtEnquete.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            EtudeEtEnqueteModel.parse_obj(publi)


def test_validate_guide_methodo():
    with open("data/processed/GuideMethodologique.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            GuideMethodologiqueModel.parse_obj(publi)


def test_validate_guide_patient():
    with open("data/processed/GuidePatient.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            GuidePatientModel.parse_obj(publi)


def test_validate_eval_tech_sante():
    with open("data/processed/EvaluationDesTechnologiesDeSante.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            EvalTechSanteModel.parse_obj(publi)


def test_validate_avis_produits():
    with open("data/processed/AVISProduitsEtPrestations.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            AvisProduitsModel.parse_obj(publi)


def test_validate_reco_vaccinale():
    with open("data/processed/RecommandationVaccinale.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            RecoVaccinaleModel.parse_obj(publi)


def test_validate_eval_pratiques():
    with open("data/processed/EvaluationDesPratiques.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            EvalPratiquesModel.parse_obj(publi)


def test_validate_recos_prof():
    with open("data/processed/RecommandationsProfessionnelles.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            RecoProfModel.parse_obj(publi)


def test_validate_eval_programmes():
    with open("data/processed/EvaluationDesProgrammesEtPolitiq.json") as filep:
        dataset = json.load(filep)
        for publi in dataset:
            EvalProgrammesPolitiqModel.parse_obj(publi)
